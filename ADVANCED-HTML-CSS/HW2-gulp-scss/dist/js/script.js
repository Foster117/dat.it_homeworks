const menuButton = document.querySelector(".menu-btn");
const dropDownMenu = document.querySelector(".header-menu");

menuButton.addEventListener("click", () => {
    menuButton.classList.toggle("active");
    if(menuButton.classList.contains("active")){
        dropDownMenu.classList.add("menu-active");
        
    }
    else{
        dropDownMenu.classList.remove("menu-active");
    }
});

