function createNewUser() {
  let fName = prompt("Enter first name:");
  let lName = prompt("Enter last name:");
  let bDay = prompt("Enter lbirthday (dd-mm-yyyy):");
  bDay = new Date(`${bDay.substr(6,4)}-${bDay.substr(3,2)}-${bDay.substr(0,2)}`);

  const newUser = {
    firstName: fName,
    lastName: lName,
    birthday: bDay,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge: function(){
      let nowDate = new Date();
      let tempDate = new Date(this.birthday);
      let userAge = nowDate.getFullYear() - this.birthday.getFullYear();
      return nowDate.setFullYear(1970) < tempDate.setFullYear(1970) ? userAge - 1 : userAge;
    },
    getPassword: function(){
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
    } 
  };
  return newUser;
}

let newUser = createNewUser();
console.log(newUser.firstName);
console.log(newUser.lastName);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
