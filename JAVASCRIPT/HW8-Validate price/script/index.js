function OnBlur() {
    if (input.value < 0) {
        input.style.border = "1px solid red";
        const invalidMessage = document.createElement("span");
        invalidMessage.innerText = "Please enter correct price";
        document.body.append(invalidMessage);
        return;
    }
    const span = document.createElement("span");
    span.style.border = "1px solid gray";
    span.style.borderRadius = "20px";
    span.style.padding = "5px";
    span.innerText = `Текущая цена: ${input.value}`;
    const button = document.createElement("button");
    button.innerText = "x";
    button.style.marginLeft = "4px";
    button.style.border = "none";
    button.style.borderRadius = "10px";
    button.style.outline = "none";
    button.addEventListener("click", () => {
        button.closest("span").remove();
        input.value = "";
        input.style.color = "black"
    });
    span.append(button);
    document.body.prepend(span);
    input.style.color = "green";
}

const input = document.querySelector("#price-input");
input.style.border = "1px solid black"
input.style.outline = "none";
input.style.boxSizing = "border-box";
input.style.width = "200px";
input.style.height = "25px";
input.style.borderRadius = "4px";

input.addEventListener("focus", () => { input.style.border = "2px solid green" });
input.addEventListener("blur", () => { input.style.border = "1px solid black"; OnBlur(); });