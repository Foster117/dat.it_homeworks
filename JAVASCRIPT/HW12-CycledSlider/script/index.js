function fadeOut(picture){
    let opacity = 1;
    const timer = setInterval(() => {
        if(opacity <= 0.2){
            clearInterval(timer);
            picture.classList.remove("active-image");
        }
        picture.style.opacity = opacity;
        opacity -= 0.1;
    }, 50);
}
function fadeIn(picture){
    let opacity = 0;
    picture.classList.add("active-image");
    const timer = setInterval(() => {
        if(opacity >= 1){
            clearInterval(timer);
        }
        picture.style.opacity = opacity;
        opacity += 0.1;
    }, 50);
}

function slider(){
    fadeOut(pictures[counter])
    setTimeout(() => {
        counter++;
        if(counter === pictures.length){
            counter = 0;
        }
        pictures[counter].style.opacity = 0;
        fadeIn(pictures[counter]);
    }, 500);

}

const pictures = document.querySelectorAll(".image-to-show");
const timeElem = document.getElementById('time');
let date = new Date();
let counter = 0;
let prevImage;
let intId

intId = setInterval(slider, 3000);

document.getElementById("stop").addEventListener("click", () => clearInterval(intId));
document.getElementById("start").addEventListener("click", () => {
    clearInterval(intId);
    intId = setInterval(slider, 3000);
});
