let userName, userAge, tempName, tempAge;

while (true) {
    userName = prompt("Enter Your Name:", tempName);
    userAge = prompt("Enter Your Age:", tempAge);

    if (userName === "" || userAge === "" || Number.isNaN(Number(userAge))) {
        tempName = userName;
        tempAge = userAge;
        continue;        
    }
    break;
}

if (userAge < 18) {
    alert("You are not allowed to visit this website.");
}
else if (userAge >= 18 && userAge <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert(`Welcome, ${userName}.`);
    }
    else {
        alert("You are not allowed to visit this website.");
    }
}
else {
    alert(`Welcome, ${userName}.`);
}
