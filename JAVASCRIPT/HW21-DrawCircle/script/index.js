let diameter;

function getRandomColor() {
    return `hsl(${Math.floor(Math.random() * (360 - 1 + 1)) + 1}, 100%, 50%)`;
}

function hideCircle(event) {
    if(event.target.className === "circle"){
        event.target.style.display = "none";
    }
}

function drawCircles() {
    const container = document.createElement("div");
    for (let j = 0; j < 10; j++) {
        for (let index = 0; index < 10; index++) {
            const circleDiv = document.createElement("div");
            circleDiv.style.width = diameter + "px";
            circleDiv.style.height = diameter + "px";
            circleDiv.style.borderRadius = diameter + "px";
            circleDiv.style.backgroundColor = getRandomColor();
            circleDiv.classList.add("circle");
            container.append(circleDiv);
        }
        const clear = document.createElement("div");
        clear.style.clear = "both";
        container.append(clear);
    }
    document.body.append(container);
    container.addEventListener("click", hideCircle);
}

function showInput() {
    const div = document.createElement("div");
    div.classList.add("inputDiv");
    document.body.append(div);
    const inp = document.createElement("input")
    inp.setAttribute("type", "text");
    inp.setAttribute("id", "butDiameter")
    div.append(inp);
    const inpBut = document.createElement("input");
    inpBut.setAttribute("type", "button");
    inpBut.setAttribute("id", "inpBut");
    inpBut.setAttribute("value", "Нарисовать")
    div.append(inpBut);
    inpBut.addEventListener("click", () => {
        diameter = butDiameter.value;
        document.querySelector(".inputDiv").style.display = "none";
        button.style.display = "none";
        drawCircles();
    });   
}

window.onload = () => {
    const btn = document.createElement("input");
    btn.setAttribute("type", "button");
    btn.setAttribute("id", "button");
    btn.setAttribute("value", "Нарисовать круг");
    btn.addEventListener("click", showInput);
    document.body.prepend(btn);
}