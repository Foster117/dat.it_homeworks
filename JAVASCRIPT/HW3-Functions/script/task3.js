function countFibonacci(f0, f1, n) {
    if (n > 2) {
        return countFibonacci(f1, f0 + f1, n - 1);
    }
    else {
        return f1;
    }
}

let f0, f1, n;
f0 = 0;
f1 = 1;

n = +prompt("Enter index number:");

if (n < 0) {
    n *= -1;
}
if (Number.isInteger(n) && n != 0) {
    console.log(countFibonacci(f0, f1, n));
}
else {
    console.log("Введенное значение 'n' не является порядковым номером.");
}
