function GetFactorial(number){
    if (number === 1) {
        return 1;
    }
    return number * GetFactorial(number -1);
}

let number, tempNumber;

while (true) {
    number = prompt("Enter number:", tempNumber);
    if (number === "" || Number.isNaN(Number(number))) {
        tempNumber = number;
        continue;
    }
    break;
}

document.write(`${number}! = ${GetFactorial(number)}`);