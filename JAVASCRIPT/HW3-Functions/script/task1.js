function isValid(operand1, operand2, mathSign) {
    if (operand1 === "" || Number.isNaN(Number(operand1))) {
        return false;
    }
    if (operand2 === "" || Number.isNaN(Number(operand2))) {
        return false;
    }
    if (mathSign !== "+" && mathSign !== "-" && mathSign !== "*" && mathSign !== "/") {
        return false;
    }
    return true;
}

function Calculate(operand1, operand2, mathSign) {
    switch (mathSign) {
        case "+":
            return +operand1 + +operand2;
        case "-":
            return operand1 - operand2;
        case "*":
            return operand1 * operand2;
        case "/":
            return operand1 / operand2;
    }
}

let operand1, operand2, mathSign, tempOperand1, tempOperand2, tempMathSign;

while (true) {
    operand1 = prompt("Enter first operand:", tempOperand1);
    mathSign = prompt("Enter math operation sign:", tempMathSign);
    operand2 = prompt("Enter second operand:", tempOperand2);
    if (isValid(operand1, operand2, mathSign)) {
        break;
    }
    tempOperand1 = operand1;
    tempOperand2 = operand2;
    tempMathSign = mathSign;
}

console.log(Calculate(operand1, operand2, mathSign));