function showPassword(icon){
    const input = icon.closest("label").querySelector("input");
    if(icon.classList.contains("fa-eye-slash")){
        icon.classList.remove("fa-eye-slash");
        input.setAttribute("type", "password");
    }
    else{
        icon.classList.add("fa-eye-slash");
        input.setAttribute("type", "text");
    }
}

const icons = document.querySelectorAll("i");
const inputs = document.querySelectorAll("input");
icons[0].addEventListener("click", (event) => showPassword(event.target));
icons[1].addEventListener("click", (event) => showPassword(event.target));

document.querySelector(".btn").addEventListener("click", () => {
    if(inputs[0].value === inputs[1].value){
        document.getElementById("validation-error").innerText = "";
        alert("You are welcome");
    }
    else{
        document.getElementById("validation-error").innerText = "Нужно ввести одинаковые значения";
    }
});