function createList(array, elementToAppend){
    
    if(elementToAppend == null){
        elementToAppend = document.body;
    }
    let list = "";

    const arrayOfLi = array.map((arg) => {
        if(Array.isArray(arg)){
            let innerList = "";
            return `<ul>${createList(arg, innerList)}</ul>`;
        }
        return `<li>${arg}</li>`;
    });

    for (li of arrayOfLi) {
        list += li;
    }
    if(typeof(elementToAppend) === "string"){
        elementToAppend += list;
        return elementToAppend;
    }
    else{
        elementToAppend.innerHTML += `<ul>${list}</ul>`;
    }
}

const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", ["Toronto", "New York", ["One", "Two", "Three", [3.1, 3.2, 3.3, 3.4], "Four", "Five"], "Brovary", "Odessa"], "Lviv", "Dnieper", ["Gotham City", "Metropolys", "Smallville"]];

const container = document.querySelector("#listContainer");
createList(array);

let countdown = 3;
const p = document.createElement("p");
p.innerText = countdown;
document.body.append(p);

let timerId = setInterval(() => {p.innerText = p.innerText - 1}, 1000);
setTimeout(() => { clearInterval(timerId); document.body.innerHTML = ""; }, 3000);
