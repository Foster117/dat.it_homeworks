function CountBadGrades(student) {
  let badGradesCounter = 0;
  for (const grade in student.tabel) {
    if (student.tabel[grade] < 4) {
      badGradesCounter++;
    }
  }
  console.log("Количество оценок меньше 4: " + badGradesCounter);
  if (badGradesCounter === 0) {
    console.log("Cтудент переведен на следующий курс");
  }
}

function CountGradesAverage(student) {
  let gradesAverage = 0;
  for (const grade in student.tabel) {
    gradesAverage += student.tabel[grade];
  }
  gradesAverage /= Object.keys(student.tabel).length;
  console.log("Средний балл по предметам: " + gradesAverage);
  if (gradesAverage > 7) {
    console.log("Студенту назначена стипендия");
  }
}


const student = {
}

student.name = prompt("Enter name:");
student.lastName = prompt("Enter last name:");
student.tabel = {};

while (true) {
  let subject = prompt("Enter subject name or press cancel to finish:");
  if (subject == null) {
    break;
  }
  let grade = +prompt("Enter grade:");
  student.tabel[subject] = grade;
}

console.log(`Студент - ${student.name} ${student.lastName}`);
CountBadGrades(student);
CountGradesAverage(student);