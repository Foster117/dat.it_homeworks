function createNewUser() {
    const newUser = {};
    Object.defineProperty(newUser, 'firstName', {
        value: prompt("Enter first name:"),
        writable : false,
        configurable: true
      });
      Object.defineProperty(newUser, 'lastName', {
        value: prompt("Enter last name:"),
        writable : false,
        configurable: true
      });

      Object.defineProperty(newUser, 'SetFirstName', {
         set: function(value) {
            Object.defineProperty(newUser, 'firstName', {
                writable : true
              });
          this.firstName = value;
          Object.defineProperty(newUser, 'firstName', {
            writable : false
          });
        }
      });
      Object.defineProperty(newUser, 'SetLastName', {
        set: function(value) {
           Object.defineProperty(newUser, 'lastName', {
               writable : true
             });
         this.lastName = value;
         Object.defineProperty(newUser, 'lastName', {
           writable : false
         });
       }
     });
      

      return newUser;
}

let newUser = createNewUser();
newUser.getLogin = function () {
    return (this.firstName.substr(0, 1) + this.lastName).toLocaleLowerCase();
}
console.log(`Login -- ${newUser.getLogin()}`);

console.log(`First name -- ${newUser.firstName}, Last name -- ${newUser.lastName}`);

newUser.firstName = "Ilon";
newUser.lastName = "Mask";
console.log(`First name -- ${newUser.firstName}, Last name -- ${newUser.lastName} ----------- свойства не изменились`);

newUser.SetFirstName = "Jaque";
newUser.SetLastName = "Fresco";
console.log(`First name -- ${newUser.firstName}, Last name -- ${newUser.lastName}`);

console.log(`Login -- ${newUser.getLogin()}`);



