function slideToggle(section){
    section.toggleClass("hidden-section");
}

$(document).ready(function(){
    $(".up-button").on("click", function (event) {
        const id  = "header",
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    $(".main-menu").on("click","a", function (event) {
        event.preventDefault();
        const id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1000);
    });

    $(window).scroll(function (event) {
        let top = $(window).scrollTop();
         if(top >= $(window).height()){
         $(".up-button").css("visibility", "visible");
         } else {
            $(".up-button").css("visibility", "hidden");
         }
    });

    $(".toggle-btn").on("click", () => {
        slideToggle($(".popular-clients-section"));
    });
});