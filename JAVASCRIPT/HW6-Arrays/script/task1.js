function filterBy(array, type){
    return array.filter(element => typeof(element) !== type.toLowerCase());
}

let array = ['hello', 117, 4568n, false, 'train', undefined, 56.3, null, {name: 'Bill', age: 40}, 'guitar', 2020, ['Lina', 'Sniper', 'WD', 'Jakiro'], 900, true, function(a){return ++a}];
console.log(filterBy(array, 'Number'));