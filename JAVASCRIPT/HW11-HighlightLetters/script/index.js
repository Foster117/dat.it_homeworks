const buttons = document.querySelectorAll(".btn");
console.log(buttons);
window.addEventListener("keydown", (event) => {
    switch (event.key.toLowerCase()) {
        case "enter":
            buttons[0].focus();
            break;
        case "s":
            buttons[1].focus();
            break;
        case "e":
            buttons[2].focus();
            break;
        case "o":
            buttons[3].focus();
            break;
        case "n":
            buttons[4].focus();
            break;
        case "l":
            buttons[5].focus();
            break;
        case "z":
            buttons[6].focus();
            break;
    }
});