let m, n, isSimple;

while (true){
    m = +prompt("Enter m:");
    n = +prompt("Enter n:");
    if (m < n) {
        break;
    }
    alert("`n` must be greater than `m`.");
}

for (let i = m; i <= n; i++) {
    if (i < 2) {
        continue;
    }
    isSimple = true;
    for (let j = 2; j < i; j++) {
        if (i % j === 0) {
            isSimple = false;
            break;
        }
    }
    if (isSimple) {
        console.log(i);
    }
}