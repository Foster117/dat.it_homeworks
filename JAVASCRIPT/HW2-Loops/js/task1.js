let number;
let flag = true;

while (true) {
    number = +prompt("Enter number:");
    if (Number.isInteger(number)) {
        break;
    }
}
for (let i = 1; i <= number; i++) {
    if (i % 5 === 0) {
        flag = false;
        console.log(i);
    }
}
if (flag) {
    console.log("Sorry, no numbers.");
}